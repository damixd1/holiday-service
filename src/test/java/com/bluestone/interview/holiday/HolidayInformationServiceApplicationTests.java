package com.bluestone.interview.holiday;

import com.bluestone.interview.holiday.service.ExternalHolidayApi;
import com.bluestone.interview.holiday.service.NextHolidayFinderService;
import com.bluestone.interview.rest.model.HolidaysExternal;
import com.bluestone.interview.rest.model.NextHolidayForTwoCountries;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
public class HolidayInformationServiceApplicationTests {

    @MockBean
    private ExternalHolidayApi externalHolidayApi;

    @Test
    public void holidaysForCountriesTest() throws IOException {

        var PL2019 = convertResourceToObject("PL2019.json", HolidaysExternal.class);
        var BE2019 = convertResourceToObject("BE2019.json", HolidaysExternal.class);

        when(externalHolidayApi.getHolidaysForCountryAndYear(eq("PL"), eq(2019))).thenReturn(PL2019);
        when(externalHolidayApi.getHolidaysForCountryAndYear(eq("BE"), eq(2019))).thenReturn(BE2019);

        var nexHolidayFinderService = new NextHolidayFinderService(externalHolidayApi);

        NextHolidayForTwoCountries nextHolidayForTwoCountries =
                nexHolidayFinderService.getNextHolidayForTwoCountries("PL", "BE", "2019-04-05");

        Assert.assertNotNull(nextHolidayForTwoCountries);
        Assert.assertEquals(nextHolidayForTwoCountries.getDate(), "2019-04-21");
        Assert.assertEquals(nextHolidayForTwoCountries.getName1(), "Niedziela Wielkanocna");
        Assert.assertEquals(nextHolidayForTwoCountries.getName2(), "Pâques");
    }

    private static <T> T convertResourceToObject(String resourceName, Class<T> objectType) throws IOException {
        var classPathResource = new ClassPathResource(resourceName);
        var mapper = new ObjectMapper();
        return mapper.readValue(classPathResource.getInputStream(), objectType);
    }
}
