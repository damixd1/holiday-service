package com.bluestone.interview.holiday.restcontroller;

import com.bluestone.interview.holiday.service.NextHolidayFinder;
import com.bluestone.interview.rest.api.NextHolidayApi;
import com.bluestone.interview.rest.model.ErrorInfo;
import com.bluestone.interview.rest.model.NextHolidayForTwoCountries;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;


@RestController
public class NextHolidayRestController implements NextHolidayApi {
    private final static Logger LOG = LoggerFactory.getLogger(NextHolidayRestController.class);

    private final NextHolidayFinder nextHolidayFinder;

    @Autowired
    public NextHolidayRestController(NextHolidayFinder nextHolidayFinder) {
        this.nextHolidayFinder = nextHolidayFinder;
    }

    @Override
    @RequestMapping(value = "/next-holiday/{countryCode1}/{countryCode2}", produces = "application/json", method = RequestMethod.GET)
    public ResponseEntity<NextHolidayForTwoCountries> nextHolidayGet(
            @PathVariable("countryCode1") String countryCode1,
            @PathVariable("countryCode2") String countryCode2,
            @RequestParam(value = "after", required = false) String after) {

        LOG.debug("Received GET request with params: countryCode1={}, countryCode2={}, after={}", countryCode1, countryCode2, after);
        NextHolidayForTwoCountries result = nextHolidayFinder.getNextHolidayForTwoCountries(countryCode1, countryCode2, after);

        LOG.debug("Response has been sent, with success");
        return ResponseEntity.ok(result);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ErrorInfo> handleError(HttpServletRequest req, Exception ex) {

        LOG.error("An Exception has occurred, Reason:", ex);
        var errorInfo = new ErrorInfo();
        errorInfo.errorCause(ex.getMessage());

        return ResponseEntity.badRequest().body(errorInfo);
    }

}
