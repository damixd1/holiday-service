package com.bluestone.interview.holiday.service;

import com.bluestone.interview.rest.model.NextHolidayForTwoCountries;

public interface NextHolidayFinder {

    NextHolidayForTwoCountries getNextHolidayForTwoCountries(String country1, String country2, String date);

}
