package com.bluestone.interview.holiday.service;

import com.bluestone.interview.rest.model.HolidaysExternal;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriBuilder;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;

@Service
public class ExternalHolidayApiService implements ExternalHolidayApi {
    private final static Logger LOG = LoggerFactory.getLogger(ExternalHolidayApiService.class);

    @Value("${external.holiday.api}")
    private String EXTERNAL_HOLIDAY_API_BASE;

    private final RestTemplate restTemplate;

    @Autowired
    public ExternalHolidayApiService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Override
    public HolidaysExternal getHolidaysForCountryAndYear(String countryCode, int year) {
        LOG.debug("Creating request for external api, with params: countryCode={}, year={}", countryCode, year);

        UriBuilder uriBuilder = UriComponentsBuilder.fromUriString(EXTERNAL_HOLIDAY_API_BASE);
        URI uri = uriBuilder
                .queryParam("country", countryCode)
                .queryParam("year", year)
                .build();

        ResponseEntity<HolidaysExternal> responseEntity = restTemplate.getForEntity(uri, HolidaysExternal.class);

        return responseEntity.getBody();
    }
}
