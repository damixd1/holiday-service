package com.bluestone.interview.holiday.service;

import com.bluestone.interview.rest.model.HolidaysExternal;

public interface ExternalHolidayApi {

    HolidaysExternal getHolidaysForCountryAndYear(String countryCode, int year);
}
