package com.bluestone.interview.holiday.service;

import com.bluestone.interview.rest.model.HolidaysExternal;
import com.bluestone.interview.rest.model.HolidaysExternalItem;
import com.bluestone.interview.rest.model.NextHolidayForTwoCountries;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Collection;
import java.util.Map;
import java.util.Optional;
import java.util.TreeMap;
import java.util.stream.Collectors;

@Service
public class NextHolidayFinderService implements NextHolidayFinder {
    private final ExternalHolidayApi externalHolidayApi;

    @Autowired
    public NextHolidayFinderService(ExternalHolidayApi externalHolidayApi) {
        this.externalHolidayApi = externalHolidayApi;
    }

    @Override
    public NextHolidayForTwoCountries getNextHolidayForTwoCountries(String country1, String country2, String date) {

        LocalDate requestedDate = getRequestedDate(date);

        int year = requestedDate.getYear();

        while (true) {
            HolidaysExternal holidaysForCountry1 = externalHolidayApi.getHolidaysForCountryAndYear(
                    country1, year);

            HolidaysExternal holidaysForCountry2 = externalHolidayApi.getHolidaysForCountryAndYear(
                    country2, year);

            var nextHolidayForTwoCountries = getNextHolidayForTwoCountries(
                    requestedDate, holidaysForCountry1, holidaysForCountry2);

            if (nextHolidayForTwoCountries.isPresent()) {
                return nextHolidayForTwoCountries.get();
            }

            year++;
        }
    }

    private Optional<NextHolidayForTwoCountries> getNextHolidayForTwoCountries(LocalDate requestedDate, HolidaysExternal holidaysForCountry1, HolidaysExternal holidaysForCountry2) {
        var country1holidaysMap = getHolidaysAsMap(holidaysForCountry1, requestedDate);
        var country2holidaysMap = getHolidaysAsMap(holidaysForCountry2, requestedDate);

        for (var entry : country1holidaysMap.entrySet()) {
            if (country2holidaysMap.containsKey(entry.getKey())) {

                String holidayForCountry1 = entry.getValue();
                String holidayForCountry2 = country2holidaysMap.get(entry.getKey());

                var nextHolidayForTwoCountries = new NextHolidayForTwoCountries();
                nextHolidayForTwoCountries.setDate(DateFormatter.parseDateToString(entry.getKey()));
                nextHolidayForTwoCountries.setName1(holidayForCountry1);
                nextHolidayForTwoCountries.setName2(holidayForCountry2);
                return Optional.of(nextHolidayForTwoCountries);
            }
        }
        return Optional.empty();
    }

    private Map<LocalDate, String> getHolidaysAsMap(HolidaysExternal holidaysExternal, LocalDate requestedDate) {
         return holidaysExternal.getHolidays().values().stream()
                 .flatMap(Collection::stream)
                 .collect(Collectors.toMap(r -> DateFormatter.parseDate(r.getDate()), HolidaysExternalItem::getName, (o, n) -> n))
                 .entrySet()
                 .stream()
                 .filter(e -> e.getKey().isAfter(requestedDate))
                 .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (o, n) -> n, TreeMap::new));
    }


    private LocalDate getRequestedDate(String date) {
        if (date == null) {
            return LocalDate.now();
        }
        return DateFormatter.parseDate(date);
    }
}
