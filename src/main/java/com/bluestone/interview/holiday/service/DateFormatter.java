package com.bluestone.interview.holiday.service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

class DateFormatter {
    private final static DateTimeFormatter INTERNAL_DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    static LocalDate parseDate(String dateAsString) {
        return LocalDate.parse(dateAsString, INTERNAL_DATE_TIME_FORMATTER);
    }

    static String parseDateToString(LocalDate date) {
        return date.format(INTERNAL_DATE_TIME_FORMATTER);
    }
}
