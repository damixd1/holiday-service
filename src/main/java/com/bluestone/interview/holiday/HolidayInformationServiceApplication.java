package com.bluestone.interview.holiday;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HolidayInformationServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(HolidayInformationServiceApplication.class, args);
    }

}
